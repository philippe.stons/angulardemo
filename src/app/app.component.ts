import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'BF-Front';

  constructor(
    private authService: AuthService
  )
  {}

  public isLoggedIn()
  {
    return this.authService.getLoggedIn();
  }

  public loggedIn()
  {
    return this.authService.getLoggedIn();
  }

  public logout()
  {
    this.authService.logout();
  }
}
