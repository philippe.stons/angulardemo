import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  edit: boolean = false;
  user!: User;
  
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) 
  {
    const id = parseInt(this.activatedRoute.snapshot.paramMap.get('id') || '', 10);
    
    if(!isNaN(id))
    {
      this.edit = true;
      this._getUser(id);
    }
  }

  ngOnInit(): void {
  }

  private _getUser(id: number)
  {
    this.userService.getOneById(id).subscribe((user: User) => 
    {
      this.user = user;
    });
  }

  submit(user: User)
  {
    if(this.edit)
    {
      this.userService.update(this.user.id, user)
        .subscribe(() => this.router.navigate(['user']))
    } else 
    {
      this.userService.insert(user)
        .subscribe(() => this.router.navigate(['user']));
    }
  }
}
