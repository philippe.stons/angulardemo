import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { ProfileComponent } from '../../components/profile/profile.component';
import { UserEditComponent } from '../../components/user-edit/user-edit.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  displayColumns: string[] = ['id', 'username', 'roles', 'actions'];
  dataSource!: MatTableDataSource<User>;
  userList!: User[];
  
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    
  }

  ngOnInit(): void {
    this.refresh();
  }

  private refresh()
  {
    this.userService.getAll()
      .subscribe((users) => 
      {
        this.userList = users;
        this.updateDataSource();
      })
  }

  private updateDataSource()
  {
    this.dataSource = new MatTableDataSource(this.userList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  detail(user: User)
  {
    const dlg = this.dialog.open(ProfileComponent, { data: user });
  }

  edit(user: User)
  {
    //this.router.navigate(['user/edit/' + user.id]);
    const dlg = this.dialog.open(UserEditComponent, { data: user });

    dlg.beforeClosed().subscribe((res: User) => 
      {
        if(res)
        {
          console.log(res);
          this.userService.update(res.id, res)
            .subscribe(
              () => this.refresh()
            );
        }
      });
  }

  delete(user: User)
  {
    if(this.authService.isCurrentUser(user))
    {
      this.snackBar.open("Are you sure?", 'yes', { duration: 3000 }).onAction()
        .subscribe(() => {
          this._delete(user.id).subscribe(() => 
          {
            this.authService.logout();
          });
        });
    } else 
    {
      this._delete(user.id).subscribe(() => 
      {
        this.refresh();
      });
    }
  }

  private _delete(id: number)
  {
    return this.userService.delete(id);
  }

  create()
  {
    //this.router.navigate(['user/add']);
    const dlg = this.dialog.open(UserEditComponent, { data: null });

    dlg.beforeClosed().subscribe((res: User) => 
      {
        if(res)
        {
          console.log(res);
          this.userService.insert(res)
            .subscribe(
              () => this.refresh()
            );
        }
      });
  }

  create2()
  {
    this.router.navigate(['user/add']);
  }

  isAdmin()
  {
    return this.authService.isAdmin();
  }

}
