import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  userForm: FormGroup;
  //usernameCtl: FormControl;
  passwordCtl: FormControl;
  rolesCtl: FormControl;
  rolesList = ['ADMIN', 'USER'];
  edit: boolean = false;
  @Input() user!: User;
  @Output() userEvent = new EventEmitter<User>();

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
  ) 
  {
    //this.usernameCtl = this.fb.control({value: null, disabled: this.user ? true : false}, [Validators.required, 
    //  Validators.minLength(4), Validators.maxLength(255)]);
    //this.passwordCtl = this.fb.control(null, [Validators.required,
    //  Validators.minLength(8), Validators.maxLength(64), 
    //  Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*.,?]).+$'),
    //  this.checkSimplePassword(), 
    //  this.checkPasswordNotUsername()]);
    //this.rolesCtl = this.fb.control(null, Validators.required);
    
    this.userForm = this.fb.group({
      username: [{value: null, disabled: this.user ? true : false},  [Validators.required, 
        Validators.minLength(4), Validators.maxLength(255)]],//this.usernameCtl,
      password: [null, [Validators.required,
        Validators.minLength(8), Validators.maxLength(64), 
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*.,?]).+$'),
        this.checkSimplePassword(), this.checkPasswordNotUsername()]],
      roles: [null, [Validators.required]]},
      {  }); //Validator sur un FormGroup.
  }

  ngOnInit(): void {
  }

  public checkSimplePassword(): ValidatorFn
  {
    return (control: AbstractControl): ValidationErrors | null => 
    {
      const password = control.value;

      let error = { pwdSimple: {value: 'this password is too simple!'} };

      if(password == "P@s5w0rd")
      {
        return error;
      }

      return null;
    }
  }

  public checkPasswordNotUsername(): ValidatorFn 
  {
    return (control: AbstractControl): ValidationErrors | null =>
    {
      const password = control.value;
      const username = this.userForm?.get('username')?.value;

      let error = { pwdEqUsername: { value: "password and username are equals" } };

      if(password != null && password == username)
      {
        //error.pwdEqUsername = true;
        return error;
      }

      return null;
    }
  }

  submit()
  {
    if(this.userForm.valid)
    {
      const user = this.userForm.value as User;

      if(this.user)
      {
        user.id = this.user.id;
      }

      this.userEvent.emit(user);
    }
  }
}
