import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from 'src/app/guard/admin.guard';
import { AuthGuard } from 'src/app/guard/auth.guard';
import { UserListComponent } from './pages/user-list/user-list.component';
import { UserViewComponent } from './pages/user-view/user-view.component';
import { UserComponent } from './pages/user/user.component';

const routes: Routes = [
  { path: '', canActivate: [AuthGuard], component: UserListComponent },
  { path: 'add', canActivate: [AdminGuard], component: UserComponent },
  { path: 'edit/:id', canActivate: [AdminGuard], component: UserComponent },
  { path: ':id', component: UserViewComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
