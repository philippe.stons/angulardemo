import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VoitureRoutingModule } from './voiture-routing.module';
import { VoitureComponent } from './pages/voiture/voiture.component';
import { VoitureListComponent } from './pages/voiture-list/voiture-list.component';
import { VoitureViewComponent } from './pages/voiture-view/voiture-view.component';
import { DetailComponent } from './components/detail/detail.component';
import { VoitureEditDialogComponent } from './components/voiture-edit-dialog/voiture-edit-dialog.component';
import { VoitureFormComponent } from './components/voiture-form/voiture-form.component';
import { VoitureDetailComponent } from './components/voiture-detail/voiture-detail.component';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [
    VoitureComponent,
    VoitureListComponent,
    VoitureViewComponent,
    DetailComponent,
    VoitureEditDialogComponent,
    VoitureFormComponent,
    VoitureDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    VoitureRoutingModule
  ]
})
export class VoitureModule { }
