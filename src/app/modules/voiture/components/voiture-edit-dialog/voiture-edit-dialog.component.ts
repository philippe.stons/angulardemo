import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Voiture } from 'src/app/models/voiture.model';

@Component({
  selector: 'app-voiture-edit-dialog',
  templateUrl: './voiture-edit-dialog.component.html',
  styleUrls: ['./voiture-edit-dialog.component.scss']
})
export class VoitureEditDialogComponent implements OnInit {
  voiture: Voiture;

  constructor(
    public dialogRef: MatDialogRef<VoitureEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Voiture
  ) {
    this.voiture = data;
  }

  ngOnInit(): void {
  }

  closeDialog(voiture: Voiture)
  {
    this.dialogRef.close(voiture);
  }
}
