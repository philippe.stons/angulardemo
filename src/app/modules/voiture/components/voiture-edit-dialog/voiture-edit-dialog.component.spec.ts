import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VoitureEditDialogComponent } from './voiture-edit-dialog.component';

describe('VoitureEditDialogComponent', () => {
  let component: VoitureEditDialogComponent;
  let fixture: ComponentFixture<VoitureEditDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VoitureEditDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VoitureEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
