import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Voiture } from 'src/app/models/voiture.model';
import { VoitureService } from 'src/app/services/voiture.service';

@Component({
  selector: 'app-voiture-form',
  templateUrl: './voiture-form.component.html',
  styleUrls: ['./voiture-form.component.scss']
})
export class VoitureFormComponent implements OnInit {
  voitureForm: FormGroup;
  edit: boolean;
  @Input() voiture: Voiture;
  @Output() voitureEvent = new EventEmitter<Voiture>();

  constructor(
    private fb: FormBuilder,
    private voitureService: VoitureService
  ) 
  {
    this.voitureForm = this.fb.group({
      marque: [null, [Validators.required]],
      modele: [null, [Validators.required]],
      puissance: [null, [Validators.required, Validators.min(0)]]
    });
  }
  
  ngOnInit(): void {
    if(this.voiture)
    {
      console.log(this.voiture);
      this.voitureForm.patchValue(this.voiture);
    }
  }

  submit()
  {
    if(this.voitureForm.valid)
    {
      let voiture = this.voitureForm.value as Voiture;

      if(this.voiture)
      {
        voiture.id = this.voiture.id;
      }

      this.voitureEvent.emit(voiture);
    }
  }
}
