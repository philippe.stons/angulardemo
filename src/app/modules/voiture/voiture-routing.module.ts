import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from 'src/app/guard/admin.guard';
import { AuthGuard } from 'src/app/guard/auth.guard';
import { VoitureListComponent } from './pages/voiture-list/voiture-list.component';
import { VoitureViewComponent } from './pages/voiture-view/voiture-view.component';
import { VoitureComponent } from './pages/voiture/voiture.component';


const routes: Routes = [
  { path: '', canActivate: [AuthGuard], component: VoitureListComponent },
  { path: 'add', canActivate: [AdminGuard], component: VoitureComponent },
  { path: 'edit/:id', canActivate: [AdminGuard], component: VoitureComponent },
  { path: ':id', component: VoitureViewComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VoitureRoutingModule { }