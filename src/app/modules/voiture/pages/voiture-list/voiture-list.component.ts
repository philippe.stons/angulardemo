import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Voiture } from 'src/app/models/voiture.model';
import { AuthService } from 'src/app/services/auth.service';
import { VoitureService } from 'src/app/services/voiture.service';
import { VoitureEditDialogComponent } from '../../components/voiture-edit-dialog/voiture-edit-dialog.component';

@Component({
  selector: 'app-voiture-list',
  templateUrl: './voiture-list.component.html',
  styleUrls: ['./voiture-list.component.scss']
})
export class VoitureListComponent implements OnInit {
  displayColumns: string[] = ['id', 'marque', 'modele', 'puissance', 'actions'];
  dataSource!: MatTableDataSource<Voiture>;
  voitureList!: Voiture[];
  
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private voitureService: VoitureService,
    private authService: AuthService,
    private snackbar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.refresh();
  }

  private refresh()
  {
    this.voitureService.getAll()
      .subscribe((voitures) => 
      {
        this.voitureList = voitures;
        this.updateDataSource();
      }
    );
  }

  private updateDataSource()
  {
    this.dataSource = new MatTableDataSource(this.voitureList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public detail(voiture: Voiture)
  {
    
  }

  public edit(voiture: Voiture)
  {
    const dlg = this.dialog.open(VoitureEditDialogComponent, { data: voiture });

    dlg.beforeClosed().subscribe(
      (voiture: Voiture) => 
      {
        this.voitureService.update(voiture.id, voiture)
          .subscribe(
            () => this.refresh()
          );
      }
    )
  }

  public create()
  {
    const dlg = this.dialog.open(VoitureEditDialogComponent, { data: null });

    dlg.beforeClosed().subscribe(
      (voiture: Voiture) => 
      {
        this.voitureService.insert(voiture)
          .subscribe(
            () => this.refresh()
          );
      }
    )
  }

  public delete(voiture: Voiture)
  {
    this.snackbar.open("Are you sure ?", 'yes',{ duration: 3000 }).onAction()
      .subscribe(() => 
      {
        this.voitureService.delete(voiture.id)
          .subscribe(() => this.refresh());
      }
    )
  }
}
