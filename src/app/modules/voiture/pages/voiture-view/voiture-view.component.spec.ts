import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VoitureViewComponent } from './voiture-view.component';

describe('VoitureViewComponent', () => {
  let component: VoitureViewComponent;
  let fixture: ComponentFixture<VoitureViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VoitureViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VoitureViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
