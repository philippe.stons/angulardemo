import { Injectable } from '@angular/core';
import { Voiture } from '../models/voiture.model';
import { CRUD, CurdConfig } from './crud';
import { ServerService } from './server.service';

const config: CurdConfig = { path: 'voiture' };

@Injectable({
  providedIn: 'root'
})
export class VoitureService extends CRUD<Voiture> {

  constructor(
    protected server: ServerService
  ) 
  {
    super(server, config);
  }
}
