import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { CRUD, CurdConfig } from './crud';
import { ServerService } from './server.service';

const config: CurdConfig = { path: 'user' };

@Injectable({
  providedIn: 'root'
})
export class UserService extends CRUD<User> {

  constructor(
    protected server: ServerService
  ) { 
    super(server, config);
  }
}
