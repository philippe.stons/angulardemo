import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'user', loadChildren: () => import('./modules/user/user.module')
      .then(t => t.UserModule) },
  { path: 'voiture', loadChildren: () => import('./modules/voiture/voiture.module')
      .then(t => t.VoitureModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
